// Arquivo: src/main/java/com/exemplo/meuapp/controller/FootballController.java

package com.exemplo.meuapp.controller;

import com.exemplo.meuapp.model.Match;
import com.exemplo.meuapp.service.FootballService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/football")
public class FootballController {

    private final FootballService footballService;

    @Autowired
    public FootballController(FootballService footballService) {
        this.footballService = footballService;
    }

    @GetMapping("/matches")
    public ResponseEntity<List<Match>> getMatches() {
        // Implementação para obter partidas (GET)
        List<Match> matches = footballService.getMatches();
        return ResponseEntity.ok(matches);
    }

    @PostMapping("/process")
    public ResponseEntity<String> processRequest(@RequestBody RequestData requestData) {
        // Implementação para processar dados (POST)
        footballService.processData(requestData);
        return ResponseEntity.ok("Data processed successfully");
    }

    @GetMapping("/ajuda")
    public String getHelp() {
        return "{\n" +
                "  \"estudante\": \"seu nome\",\n" +
                "  \"projeto\": \"nome do projeto\"\n" +
                "}";
    }
}
