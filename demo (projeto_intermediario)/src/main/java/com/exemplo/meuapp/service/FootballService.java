package com.exemplo.meuapp.service;

import com.exemplo.meuapp.model.Match;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

@Service
public class FootballService {

    private final RestTemplate restTemplate;

    @Value("${api-football.apiKey}")
    private String apiKey;

    @Value("${api-football.baseUrl}")
    private String baseUrl;

    public FootballService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Match> getMatches() {
        String url = baseUrl + "/matches";
        ResponseEntity<List<Match>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Match>>() {});
        return response.getBody();
    }

    public void processData(RequestData requestData) {
        // Implemente a lógica para processar os dados recebidos
    }
}
