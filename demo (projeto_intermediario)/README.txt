Endpoints
Consulta de Partidas
Retorna informações sobre partidas de futebol.

Método: GET
Endpoint: http://localhost:8080/api/football/matches
Processamento de Dados
Envia dados para processamento relacionados às partidas de futebol.

Método: POST
Endpoint: http://localhost:8080/api/football/process
Exemplo de requisição POST:

json
Copy code
{
"id": 33,
"title": "Produto Teste",
"description": "Descrição do produto teste",
"price": 88.99,
"discountPercentage": 0.0,
"rating": 4,
"stock": 200,
"brand": "Marca Teste",
"category": "Categoria Teste",
"thumbnail": "",
"images": [""]
}
Ajuda
Retorna informações de ajuda sobre o serviço.

Método: GET
Endpoint: http://localhost:8080/api/football/ajuda