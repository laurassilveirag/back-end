package com.example;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class JogadorGerador {
    private String[] listaNomes;
    private String[] listaSobrenomes;
    private String[] listaPosicoes;
    private String[] listaClubes;

    public JogadorGerador() {
        this.listaNomes = obterLista("nomes");
        this.listaSobrenomes = obterLista("sobrenomes");
        this.listaPosicoes = obterLista("posicoes");
        this.listaClubes = obterLista("clubes");
    }

    public Jogador geraJogador() {
        String nome = escolherDaLista(listaNomes);
        String sobrenome = escolherDaLista(listaSobrenomes);
        String posicao = escolherDaLista(listaPosicoes);
        String clube = escolherDaLista(listaClubes);
        Integer idade = gerarNumeroAleatorio(17, 45);
        Jogador jogador = new Jogador(
                nome,
                sobrenome,
                posicao,
                clube,
                idade
        );
        return jogador;
    }

    public static int gerarNumeroAleatorio(int from, int to) {
        return (int) (from + (Math.random() * (to - from)));
    }

    public String escolherDaLista(String[] lista) {
        int randomIndex = (int) Math.floor(Math.random() * lista.length);
        String palavra = lista[randomIndex];
        return palavra;
    }

    private String[] obterLista(String nomeEntidade) {
        try {
            System.out.println("Obtendo lista: " + nomeEntidade);
            String uri = "https://venson.net.br/resources/data/" + nomeEntidade + ".txt";
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            String texto = response.body();
            String[] lista = texto.split("\n");
            return lista;
        } catch (Exception exception) {
            System.out.println("Erro durante o download");
            System.out.println(exception.getMessage());
            return new String[0];
        }
    }
}
