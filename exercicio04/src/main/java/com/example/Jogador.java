package com.example;

public class Atleta {
    private String nome;
    private String sobrenome;
    private String posicao;
    private String equipe;
    private Integer idade;

    public Atleta(String nome, String sobrenome, String posicao, String equipe, Integer idade) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.posicao = posicao;
        this.equipe = equipe;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return this.nome + " "
                + this.sobrenome + " é um atleta brasileiro, "
                + "com " + this.idade + " anos de idade, "
                + "atuando na posição de "
                + this.posicao + ", representando "
                + "a equipe do " + this.equipe + ".";
    }
}
